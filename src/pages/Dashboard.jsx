import React from 'react'
import { GoPrimitiveDot } from 'react-icons/go';

import { GridComponent, ColumnsDirective, ColumnDirective, Resize, Sort, ContextMenu, Filter, Page, ExcelExport, PdfExport, Edit, Inject } from '@syncfusion/ej2-react-grids';

import { earningData, SparklineAreaData, ecomPieChartData, ecomPieChartData1, ordersData, contextMenuItems, ordersGrid } from '../data/dummy';
import { useStateContext } from '../context/ContextProvider';

import { SparkLine, Pie } from '../components'

const Dashboard = () => {
  
  return (
    <>
      {/* Dashboard */}
      <div className='mt-20 md:mt-5'>
        <div className='flex flex-wrap justify-center'>
          {/* Card */}
          {earningData.map((item) => (
            <div key={item.title}
            className='bg-white dark:text-gray-200 dark:bg-secondary-dark-bg p-4 mx-3 my-2 rounded-2xl w-96 sm:w-72 2xl:w-64 shadow-lg'
            >
              <div className='flex items-center'>
                <button type='button'
                className='text-2xl rounded-full p-2 hover:drop-shadow-xl'
                >
                <img style={{ width: 50, height: 50 }} src={item.bgImage} alt=""/>
                </button>
                <p className='ml-2'>
                  {item.title}
                </p>      
              </div>
              <div className=' flex item-center justify-center mt-2'>
                <p className=''>
                  <span className='text-lg font-semibold'>
                  ₱ {item.amount}
                  </span>
                </p>
                <p className='ml-2'>
                  <span className='text-sm font-semibold text-green-600'>
                    {item.percentage}
                  </span>
                </p>
              </div>
            </div>
          ))}
        </div>
        <div className='mt-6 flex flex-wrap justify-center gap-11 mx-20'>
          <div className='px-8 sm:px-16 bg-white rounded-xl shadow-lg'>
            {/* Pie Chart */}
            <Pie 
              id="pie-chart"
              data={ecomPieChartData1}
              legendVisiblity={true}
              height="300"
              startAngle={0}
              endAngle={360}
              radius="75%"
            />
          </div>
          <div className='px-8 sm:px-16 bg-white rounded-xl shadow-lg'>
            {/* Half Pie Chart */}
            <Pie 
              id="pie-chart1"
              data={ecomPieChartData}
              legendVisiblity={true}
              height="300"
              startAngle={270}
              endAngle={90}
              radius="100%"
            />
          </div>
          <div className='px-8 sm:px-16 bg-white rounded-xl shadow-lg'>
            {/* Half Pie Chart */}
            <Pie 
              id="pie-chart2"
              data={ecomPieChartData}
              legendVisiblity={true}
              height="300"
              startAngle={270}
              endAngle={90}
              radius="100%"
            />
          </div>
        </div>
          <div className='flex flex-wrap justify-center'>
            <div className='bg-white rounded-xl shadow-lg w-smListView p-10 m-10 sm:w-5/6 md:w-mdListView lg:w-lgListView xl:w-xlListView 2xl:w-xxlListView 3xl:w-xxxlListView'>
              <span className='font-bold'>Recent Cheques</span>
              <GridComponent
                id='gridcomp'
                dataSource={ordersData}
                allowPaging
                allowSorting
              >
                <ColumnsDirective>
                {ordersGrid.map((item, index) => (
                  <ColumnDirective key={index} {...item} />
                ))}
                </ColumnsDirective>
                {/* <Inject services={[Resize, Sort, ContextMenu, Filter, Page, ExcelExport, Edit, PdfExport]}/> */}
              </GridComponent>
            </div>
          </div>
      </div>
    </>
  )
}

export default Dashboard