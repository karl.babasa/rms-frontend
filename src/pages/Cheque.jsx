import React from 'react'
import { GridComponent, ColumnsDirective, ColumnDirective, Resize, Sort, ContextMenu, Filter, Page, ExcelExport, PdfExport, Edit, Inject, Toolbar } from '@syncfusion/ej2-react-grids';
import { Header } from '../components';

import { chequeData, chequeGrid } from '../data/dummy';

const Cheque = () => {
  const toolbarOptions = ['Search', 'Print'];
  return (
    <div className='flex flex-wrap justify-center'>
      <div className='bg-white rounded-xl shadow-lg w-smListView p-10 m-10 sm:w-5/6 md:w-mdListView lg:w-lgListView xl:w-xlListView 2xl:w-xxlListView 3xl:w-xxxlListView'>
        <Header category="Page" title="Cheques" />
        <GridComponent
        id="gridcomp"
        dataSource={chequeData}
        allowPaging
        allowSorting
        toolbar={toolbarOptions}
        
        >
          <ColumnsDirective>
            {chequeGrid.map((item, index) => (
              <ColumnDirective key={index} {...item} />
            ))}
          </ColumnsDirective>
          <Inject services={[Resize, Sort, ContextMenu, Filter, Page, ExcelExport, Edit, PdfExport, Toolbar ]}/>
        </GridComponent>
      </div>
    </div>
  )
}

export default Cheque