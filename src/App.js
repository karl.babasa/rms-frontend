import React, { useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { FiSettings } from 'react-icons/fi';
import { TooltipComponent } from '@syncfusion/ej2-react-popups';

import { Navbar, Footer, Sidebar, ThemeSettings, Pie } from './components';
import { Dashboard, Lots, Improvement, Cheque } from './pages';

import { useStateContext } from './context/ContextProvider';

import './App.css';

const App = () => {
  
  const { activeMenu } = useStateContext();

  return (
    <div>
      <BrowserRouter>
        <div className='flex relative dark:bg-main-dark-bg'>
          <div className='fixed right-4 bottom-4' style={{ zIndex: '1000'}}>
            <TooltipComponent content="Settings" position="Top">
              <button type='button'
              className='text-3xl p-3 hover: drop-shadow-xl hover:bg-light-gray text-white'
              style={{background: 'green',
              borderRadius: '50%'}}>
                <FiSettings />
              </button>
            </TooltipComponent>
          </div>
          {activeMenu ? (
            <div className='w-72 fixed sidebar dark:bg-secondary-dark-bg bg-white'>
              <Sidebar />
            </div>
          ) : (
            <div className='w-0 dark:bg-secondary-dark-bg'>
              <Sidebar />
            </div>
          )}
          <div className={
            activeMenu
            ? 'dark:bg-main-dark-bg  bg-main-bg min-h-screen md:ml-72 w-full  '
            : 'bg-main-bg dark:bg-main-dark-bg  w-full min-h-screen flex-2 '
          }>
            <div className='fixed md:static bg-main-bg dark:bg-main-dark-bg navbar w-full'>
              <Navbar />
            </div>

          <div>
            <Routes>
              {/* Dashboard */}
              <Route path="/" element={<Dashboard />} />
              <Route path="/home" element={<Dashboard />} />

              {/* Pages */}
              <Route path="/lots" element={<Lots />} />
              <Route path="/improvements" element={<Improvement />} />
              <Route path="/cheques" element={<Cheque />} />
            </Routes>
          </div>
        </div>
        </div>
      </BrowserRouter>
    </div>
  )
}

export default App